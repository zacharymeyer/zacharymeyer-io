---
title: "I Moved My Website Off Ghost's Blogging Platform"
date: 2022-06-02T11:11:43-07:00
draft: false
tags:
  - golang
  - blog
categories:
  - Technology
  - Personal
---

First and foremost, I want to state that Ghost has been an absolute pleasure to work on. Ghost has a ton of amazing features that allows you to rapidly crank out content and best of all it is an open-source project. Their platform is aimed at being extremely easy to setup and use, so you are able to focus on what matters the most - content creation. They even have the ability to send emails out to people who have signed up when you post a new article and they support various ways to monetize your site. However, there were a few things that led me to move away from Ghost.

## Why I Left Ghost

The first is that I wanted more control over my website. Ghost offers a great deal of customization, but it ultimately doesn't allow for as much control as something like Hugo. For example, with Hugo I can easily deploy my website to any free hosting service such as GitHub Pages or Cloudflare Pages and rapidly develop templates to generate a static site. With the amount of writing I do and how few features I need to produce content for such a small amount of users that read the things that I post, Ghost became a bit of an overkill. If their platform was overkill, why am I paying $25/month for something I don't necessarily need? I am aware that I could have just deployed one of their official docker images to my own DigitalOcean droplet and called it a day but then again I asked myself what do I need to write content for my website?

## What I'm Using Now

I'm now using Cloudflare Pages to host my website which costs me nothing since I am using their free plan and it was extremely easy to add continuous deployment and other neat features like continuous integration to my repository (If you're interested in an article about this please let me know). For those unfamiliar with CloudFlare, they are primarily a CDN but they have recently added support for static site hosting on their free plan. I'm using the excellent Hugo static site generator to generate my website templates and content which is extremely fast since it is written in Go (and I LOVE Golang). Overall, I'm very happy with this move as it allows me to have a considerable amount of control over how my website looks and feels without having to pay an exorbitant amount of money for something that I don't need.

## Conclusion

If you're currently using Ghost's platform or another Content Management System (CMS) and you don't need a bunch of features or you're relatively new to blogging, I would urge you to consider a static site generator such as Hugo or Jekyll. Not only will it save you a lot of money in the long run, but it will also give you a lot more control over your website. Ghost is still an amazing platform and I have nothing negative to say about it, but for me, it ultimately became unnecessary for what I needed.

I hope this helps anyone undecided about making the switch from a CMS such as WordPress/Ghost to a static site generator like Hugo or if you were just curious about my reasoning behind it. If you have any questions, feel free to reach out to me on Twitter!

### References & Links

Posting the links below for anybody that may be interested in checking out either of the 2 mentioned:

- [Hugo](https://gohugo.io)
- [Ghost](https://ghost.io)
