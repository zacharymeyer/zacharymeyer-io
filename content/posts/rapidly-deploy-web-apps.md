---
title: "Rapidly Deploy Web Applications With a Continuous Delivery Pipeline"
date: 2022-06-12T00:11:01-07:00
draft: false
tags:
  - GitHub
  - AWS
  - CI
  - CD
  - SolidJS
  - Node
categories:
  - CI/CD
  - Technology
---

Continuous Integration and Continuous Deployment (CI/CD) is a software engineering practice that allows you or a team of engineers to automate the process of delivering software from development to production. In this article, I will be building a simple web application using solidjs and automating the process of deployment. There will be little actual coding and more just a simple project setup. My goal is to increase the velocity of delivering software while being able to maintain quality and minimize errors. I will be using the following services to achieve this:

- **GitHub**: For source control management and deployment
- **Amazon S3**: For storing our static web application along with any assets
- **Amazon CloudFront**: For distributing our static web content

If you're following along to this article, I will assume that you already have a GitHub, AWS, and Node.

If you do not have any of the following you can find out more about them below:

- [GitHub](https://github.com)
- [AWS](https://aws.amazon.com/free)
- [Node](https://nodejs.org/en/) (I recommend actually using [NVM](https://github.com/nvm-sh/nvm) to manage your Node versions)

You can find the published GitHub repo [here](https://github.com/itiszac/solid-ci-cd) and the website deployed [here](https://ds9tkanvjaq7x.cloudfront.net/)

## Creating A Simple Solid App

This will be a relatively straightforward topic. I won't go into much detail on how solid works and we won't make any direct changes to the generated template. For more information on solid, you can check out the [docs](https://www.solidjs.com/docs/latest/api). I chose to do this article using the solid library for a couple of different reasons - It's not React and because Solid seems to be a pretty interesting frontend library. I do think I should make it clear that you can likely follow along with any library of your choice (ex: React).

First, I will need to generate my project using solid's starter templates. The command is as follows:

```bash
npx degit solidjs/templates/ts solid-ci-cd
cd solid-ci-cd/
```

Working with software it is important to never make assumptions, theoretically I should be running this project and at least attempting to build it but I will "assume" that everything is working as is.

I will then want to create a new repository on GitHub. For this article, I will name my repository **solid-ci-cd**. Once doing so I head back over to my terminal and enter the following commands to start tracking my project with git and then push to the remote repository.

```bash
git add .
git commit -m "initial commit"
git remote add origin git@github.com:itiszac/solid-ci-cd.git
git push -u origin main
```

## Building the CI/CD Pipeline

Now onto my favorite part of this article and probably the most important - CI/CD. Starting my first job as a Software Engineer I was introduced to an array of topics including CI/CD by a friend at work who led our Infrastructure team. Ever since, I have realized the importance of making your job easier by automating as much as you can. That being said, It should be of no surprise I'm a huge fan of automating as much of the software development process as possible. Not only does this minimize human error, but it also speeds up the process by allowing you to focus on more important tasks like solving actual problems.

For my little solid app, I will be using GitHub's native CI tool called Actions. It's relatively simple to use and can be quite powerful to define automated workflows. The only thing that you would need to know is a little bit of YAML. The great thing about Actions is that a lot of the heavy lifting has already been done by other talented engineers. For example, if you want to run a job to make sure that you have ran prettier on your JavaScript code you can most likely search their marketplace and find a workflow available. In this article, I won't cover many of the amazing things that you can accomplish with CI but I encourage you to read more about it. These are important quality assurance (QA) checks that will help to ensure a higher quality software when delivery to the customer (You could also think of it as ensuring a more successful career).

To get started, I will need to create a YAML file. Before doing so I need to create a folder required by GitHub "**.github/workflows**" in the root of my project and then inside of **workflows/** I will name my YAML file "**build-and-deploy.yml**". This file is where I will define the workflow. For our pipeline, I will have two jobs: build and deploy. The first job, build, is going to install all of the dependencies (npm ci) and build for production (npm run build). The second job, deploy, is going to take the production build artifacts and upload them to S3 along with registering an invalidation for caching with CloudFront.

An example of what my file would look like:

```yaml
name: Build and Deploy
on:
  push:
    branches:
      - main

env:
  AWS_ACCESS_KEY_ID: ${{ secrets.AWS_ACCESS_KEY_ID }}
  AWS_SECRET_ACCESS_KEY: ${{ secrets.AWS_SECRET_ACCESS_KEY }}
  AWS_DEFAULT_REGION: us-west-1

jobs:
  build:
    name: Build
    runs-on: ubuntu-latest
    steps:
      - name: Checkout
        uses: actions/checkout@v3
      - name: Node
        uses: actions/setup-node@v3
        with:
          node-version: 16
      - name: Install Deps
        run: npm ci
      - name: Builder
        run: npm run build
      - name: Cache
        uses: actions/cache@v3
        id: cache-build
        with:
          path: ./dist
          key: ${{ github.sha }}
  deploy:
    name: Deploy
    runs-on: ubuntu-latest
    needs: build
    steps:
      - name: AWS CLI
        uses: shinyinc/action-aws-cli@v1.2
      - name: Cache
        uses: actions/cache@v3
        id: cache-build
        with:
          path: ./dist
          key: ${{ github.sha }}
      - name: Deploy to S3
        run: aws s3 sync dist s3://${{ secrets.AWS_S3_BUCKET_NAME }}
      - name: CloudFront Validation
        run: aws cloudfront create-invalidation --distribution-id ${{ secrets.AWS_DISTRIBUTION_ID }} --paths "/index.html"
```

Now that I have finished working on this change I will then push all of the changes to my GitHub repository.

**Note**:
Essentially what I'm doing with the file above is running a job called build, this job is then doing a series of steps - Checking out the repo, Setting up Node v16, Installing dependencies, Building the app, and caching it so that the deploy job can use it. The deploy job is installing the AWS CLI action (built by a 3rd party), grabbing the cached files from the previous job, pushing the files located in the "dist" folder to my bucket, and creating an invalidation in CloudFront.

I can also view the pipeline in real-time when I check back on GitHub and go into the **Actions** item on the menu.

**Notice**:
The Deploy job fails and the Build job passes.

## Creating An S3 Bucket

Now that I've completed the CI/CD steps, it's time to create an S3 bucket. For this, I will head over to the AWS management console but you can just follow this [link](https://s3.console.aws.amazon.com/). I will go ahead and create a bucket and fill in the name. For simplicity's sake, I'm going to just name it the same as my repo "**solid-ci-cd**".

You can view the image below for a representation of what it should look like:

![Creating an S3 Bucket](create-s3-bucket.png)

I'm going to just keep all of the setting default except the ones related to public access (to ensure that the public can view these files/assets).

![Public Access](public-access.png)

After creating the bucket I need to make sure that I go to the **properties** tab and change the **static website hosting** setting to be enabled, make sure that hosting a static website is checked, and then I'm going to add the root page of my website (in my case it happens to be index.html).

You can verify your settings with the image below:

![Static Website Hosting](static-website-hosting.png)

Alright and now there is one last thing to do with the bucket before moving on. It's important to make sure that there is a **Bucket Policy** set and to do this it's within the permissions tab. The policy can be a bit overwhelming if you're not sure what you're really looking for. To make things easier I will use the **policy generator**, this allows me to easily search and select rules and then generate a policy to paste in the editor.

Below are the rules I selected:

![Policy](policy.png)

Awesome, the S3 bucket is done and now I need to generate a user that has access to this resource. IAM is a service which helps make this possible, it's used to manage access to AWS resources. You can find it by searching for it in the search bar or just following the [link](https://console.aws.amazon.com/iamv2/).

## IAM

On the side menu, there's an item named **Users** I'll go here to get started with creating a new user. You can name your user whatever, I would recommend something relating to the project you're working on. In the scope of this project, I will name the user **solid-ci-cd**, select the option **Access key - Programmatic access**, and then select next to create a policy.

For the simplicity of this article, I'm choosing 2 existing policies. I highly encourage you to research further into this and create a custom policy. The reason being is you don't want whatever resource that has access to the keys generated to have access to unwanted or unnecessary things. It's best to only give access to exactly what is being accomplished. Anyways, for the workflow to properly push files to the S3 bucket or create invalidations for caching files I will go ahead and select the **AmazonS3FullAccess** and **CloudFrontFullAcess** policies located in the existing policies tab and click next. Since we don't need to add any tags I'll skip it and copy the keys generated before closing (If you lose a secret key you will need to deactivate the user and generate a new one - they are irrecoverable).

## CloudFront

Alright, so now it's time to set up a distribution service on CloudFront. You can follow the link [here](console.aws.amazon.com/cloudfront/v3/home). When creating a distribution I'll select the S3 bucket as the origin domain. Near the bottom, I'll set default root object as index.html (this is the file we will want to serve when a user tries to access the root URL). Since I won't discuss setting up a custom domain in this article I'll keep all of the remaining settings as default and just create the distribution.

![Distribution Settings](distribution-settings.png)

So now that there is an S3 bucket and CloudFront distribution the next step will be to add all of the secrets to the GitHub repo.

**Note:**
One thing to remember is that in the Details section you will find the distribution domain name. This is of course your sites domain name given by amazon. In another article I may cover setting up a custom domain along with more jobs.

## GitHub Secrets

Finally, I will add the access and secret key that was generated when creating a user in the IAM AWS resource management service. Looking back at the CI file that was created we can see that there are 4 different secrets that need to be added to make everything work.

- **AWS_ACCESS_KEY_ID**: this was given when creating a user
- **AWS_SECRET_ACCESS_KEY**: this was given when creating a user
- **AWS_S3_BUCKET_NAME**: this is the name of the bucket
- **AWS_DISTRIBUTION_ID**: this is the id of the distribution

Once added, I should be able to rerun the workflow on GitHub and see that it is successfully passing on both jobs.

If you followed along you can go and access your S3 bucket and see the bundled files that were pushed from the pipeline. If you were to checkout the distribution domain you will then see your site live as well. :)

## Conclusion

I now have a working CI/CD pipeline that I am able to use to deploy a static web application using GitHub Actions, Amazon S3, and CloudFront. This was a relatively simple process to get set up but there is more that could be done to make this an even more efficient workflow. I encourage you to experiment with this by adding in automated testing, a custom domain, setting up a staging environment so that you're performing some sort of manual QA before deploying to production, and so on.

If you have any questions feel free to reach out to me on [Twitter](https://twitter.com/absencelul). I would love to hear feedback on the article or if there are any inaccuracies. I am always wanting to improve so I would much appreciate it!

## Resources

- [GitHub Actions -- Sharing Data Between Jobs](https://levelup.gitconnected.com/github-actions-how-to-share-data-between-jobs-fc1547defc3e)
- [Configuring OpenID Connect in Amazon Web Services](https://docs.github.com/en/actions/deployment/security-hardening-your-deployments/configuring-openid-connect-in-amazon-web-services)
- [Deplying create-react-app to S3 and CloudFront](https://wolovim.medium.com/deploying-create-react-app-to-s3-or-cloudfront-48dae4ce0af)
- [Configure a secure custom domain in CloudFront](https://davelms.medium.com/using-a-custom-domain-in-cloudfront-with-an-ssl-certificate-and-route-53-253a72f51056)
