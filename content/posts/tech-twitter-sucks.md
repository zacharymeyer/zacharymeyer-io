---
title: "Tech Twitter Sucks"
date: 2022-06-04T15:58:44-07:00
draft: false
tags:
  - twitter
  - social-media
categories:
  - Technology
  - Rant
---

It's no secret that the world of tech Twitter is in a bit of a state at the moment. There are too many influencers who are focused on generating higher conversion rates on their posts instead of producing quality content. This has led to a lot of low-quality content being produced, and it's hurting the entire community. Companies like RapidAPI and many more are jumping into the influencer space by hiring these creators as Developer Advocates. If we want to improve the state of tech Twitter, we need to encourage these companies to sponsor and/or create better content and remove ones who continue to output an array of bad or low-quality information. Ultimately, the best way to do something like this is to just stop interacting with people who post such stuff or even block them.

## What should I do?

If you're a new developer, the best way to learn is not from Twitter. There are plenty of other resources out there that can provide you with quality content. However, if you're looking for some good insights or want to stay up-to-date on the latest trends, Twitter can be a great resource. Just be sure to follow the right people and companies, and don't get caught up in the echo chamber. An easy way to spot that somebody is probably not the best person to follow is if they are consistently posting about how much money they're making in tech, how easy it is to get a job, or that all you need to do is "follow these 10 steps to become a developer in 3 months". These are not the people who are going to help you learn and grow as a developer. They will continue to use your interactions and impressions as a way to feed their ego and generate income. Instead, focus on following people who share quality content and resources that can help you improve your skills.

I believe that creators should be posting more about encouraging others to build side projects, the realistic expectations of obtaining your first job, or even showing others where they can learn a specific language/technology/[insert whatever]. There are a lot of great people doing this already, but we need more of it. We also need more people to be open and transparent about their experiences with imposter syndrome and anxiety. Too often, we see people putting on a facade that everything is perfect when it's not. This only serves to make others feel worse about themselves and their journeys.

## Reality

You may not make USD 200k at your first job and you may not ever get a job at a FAANG company. That is okay, you don't need these things to live a good and happy life. You likely won't get your first job as a developer after only 3-6 months of learning a specific language or framework. It takes time to learn and grow, and that's okay. The most important thing is that you enjoy the journey, continue to self-improve your skills, and don't give up.

## Conclusion

The only way to make tech Twitter better is by supporting quality content creators and being more open about our experiences. I encourage everybody else who doesn't want their feed flooded with unchecked lies, misinformation, or absolute bad advice that does nothing but slow the actual learning process - let's stop interacting with these people!
